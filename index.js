const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 4000

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

let products = [
  {
    id: 1,
    name: "Tarjeta Madre ASUS B450M-A II",
    price: 1699,
    image: "images/product-1.jpg" ,
    stock: 3,
  },

  {
    id: 2,
    name: "Tarjeta NVIDIA GeForce RTX 3060",
    price: 14219,
    image: "images/product-2.jpg" ,
    stock: 3,
  },

  {
    id: 3,
    name: "Procesador AMD Ryzen 7 5700G",
    price: 6369,
    image: "images/product-3.jpg" ,
    stock: 3,
  },

  {
    id: 4,
    name: "Ventilador Cooler Master",
    price: 699,
    image: "images/product-4.jpg" ,
    stock: 3,
  },

  {
    id: 5,
    name: "Computadora Gamer Xtreme",
    price: 24269,
    image: "images/product-5.jpg" ,
    stock: 20,
  },

  {
    id: 6,
    name: "Memoria RAM XPG DDR4, 16GB",
    price: 1259,
    image: "images/product-6.jpg" ,
    stock: 3,
  },
];

app.get('/api/products', (req, res) => {
  res.send(products);
});


app.post('/api/pay', (req, res) => {
  const ids = req.body;
  const produtsCopy = products.map(p => ({...p}));
  ids.forEach(id => {
    const product = produtsCopy.find(p => p.id === id);
    if (product.stock > 0 ){
      product.stock--;
    }
    else {
      throw "Sin stock";
    }
    
  });
  
  products = produtsCopy;
  res.send(products);
});

app.use("/", express.static("Frontend"));

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});